require 'watir'

browser = Watir::Browser.new :chrome

homepageURL = 'http://localhost:8080'

Given(/^I navigate to the homepage$/) do
  browser.goto homepageURL
end

Then(/^I should see "([^"]*)"$/) do |value|
  puts browser.text.include? value
end

When(/^I click on "([^"]*)"$/) do |value|
  browser.div.a(:text, value).click 
end

When(/^I fill out and submit new hero form$/) do 
  browser.text_field(:xpath => '//*[@id="main"]/div/form/div[1]/div/input').set 'https://thumbs.dreamstime.com/x/crash-test-dummy-527710.jpg'
  browser.text_field(:xpath => '//*[@id="main"]/div/form/div[2]/div/input').set 'dummy'
  browser.text_field(:xpath => '//*[@id="main"]/div/form/div[3]/div/input').set 'dummy'
  browser.text_field(:xpath => '//*[@id="main"]/div/form/div[4]/div/input').set 'dummy'
  browser.text_field(:xpath => '//*[@id="main"]/div/form/div[5]/div/input').set 'dummy'
  browser.text_field(:xpath => '//*[@id="main"]/div/form/div[6]/div/input').set 'Yes'
  browser.text_field(:xpath => '//*[@id="main"]/div/form/div[7]/div/input').set '1'
  browser.text_field(:xpath => '//*[@id="main"]/div/form/div[8]/div/input').set '1'
  browser.text_field(:xpath => '//*[@id="main"]/div/form/div[9]/div/input').set '1'
  browser.text_field(:xpath => '//*[@id="main"]/div/form/div[10]/div/input').set '1'
  browser.text_field(:xpath => '//*[@id="main"]/div/form/div[11]/div/input').set '1'
  browser.text_field(:xpath => '//*[@id="main"]/div/form/div[12]/div/input').set '1'
  browser.text_field(:xpath => '//*[@id="main"]/div/form/div[13]/div/input').set 'the dummies'
  browser.button(:text => 'Submit').click
  sleep 1
end

When(/^I should see an alert box$/) do 
  browser.alert.ok
end

When(/^I click on "([^"]*)"$/) do |value|
  browser.div.a(:text, value).click 
end