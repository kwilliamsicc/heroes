import React from 'react'
import {render, shallow} from 'enzyme';
import { expect } from 'chai';
import Footer from '../../js/src/components/Footer';

const text= "Notify Proper Authorities if Mutants Exhibit Dangerous Behavior"
describe('Footer component', () => {
    it('should have render table', () =>{
    const wrapper = shallow(<Footer />);
    expect(wrapper.text()).to.contain(text);
    })
})