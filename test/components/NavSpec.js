import React from 'react'
import {render, shallow} from 'enzyme';
import { expect, assert } from 'chai';
import Nav from '../../js/src/components/Nav';

describe('<Nav />', () => {
    it('should render 3 Link tags', () =>{
    const wrapper = shallow(<Nav />);
    const texts = wrapper.find('.navbar-brand').map(node => node.text());
    console.log(texts);
    expect(wrapper.find("Link")).to.have.length(3);
    expect(wrapper.find('.navbar-brand')).to.have.length(3);
    });
});