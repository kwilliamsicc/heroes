import React from 'react';
import { shallow } from 'enzyme';
import reducer from '../../js/src/reducers/index';
var chai= require('chai')
	, expect = chai.expect
	, should = chai.should();



describe('reducers', () =>  {
	it('should exist',() => {
		var initialState ={};
		initialState.should.be.an('object');
	});
});

describe('reducer init', () => {
	it('test INIT reducer', () => {
		var initialState= reducer([], {type: 'INIT', hero:[], heroGroup:[]});
		expect(initialState.hero).to.exist;
	});
});

const hero = {"groups":[],"abilities":{"energyProjection":2,"strength":6,"fightingSkills":7,"durability":6,"speed":6,"intelligence":5},"realName":"Danny Rand","s3ImageUrl":"http://vignette4.wikia.nocookie.net/marveldatabase/images/c/c3/Iron_Fist.png/revision/latest?cb=20081103155533","powers":"Enhanced strength, speed, stamina, durability, agility, reflexes and senses and ability to focus chi energy into punch.","uuid":"ironfist","heroName":"Iron Fist","signedAccords":"Yes"}

describe('reuducer post', () => {
	it('test POST reducer', () =>{
		var newState= reducer([], {type: 'POST',  heroGroup:hero});
		expect(newState).to.exist;
	})
})

describe('reuducer init_group', () => {
	it('test INIT_GROUP reducer', () =>{
		var newState= reducer([], {type: 'INIT_GROUP',  heroGroup:hero});
		expect(newState).to.exist;
	})
})

describe('reducer default', () =>{
	it('test default case', () =>{
		var newState= reducer([], {type: 'NONEXIST', heroGroup:hero});
		expect(newState).to.exist;
	})
})