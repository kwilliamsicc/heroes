import React from 'react'
import {render, shallow} from 'enzyme';
import { expect } from 'chai';
import Header from '../../js/src/components/Header';


describe('Header component', () => {
    it('should have render table', () =>{
    const wrapper = shallow(<Header />);
    expect(wrapper.text()).to.contain("Real Name");
    })
})