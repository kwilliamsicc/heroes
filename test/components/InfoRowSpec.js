import React from 'react';
import { render, shallow } from 'enzyme';
import { expect } from 'chai';
import InfoRow from '../../js/src/components/InfoRow';

const hero = {"groups":[],"abilities":{"energyProjection":2,"strength":6,"fightingSkills":7,"durability":6,"speed":6,"intelligence":5},"realName":"Danny Rand","s3ImageUrl":"http://vignette4.wikia.nocookie.net/marveldatabase/images/c/c3/Iron_Fist.png/revision/latest?cb=20081103155533","powers":"Enhanced strength, speed, stamina, durability, agility, reflexes and senses and ability to focus chi energy into punch.","uuid":"ironfist","heroName":"Iron Fist","signedAccords":"Yes"}

describe('InfoRow Component', () => {
  it('should have an image to display', () => {
    const wrapper = shallow(<InfoRow heroInfo={hero}/>);
    expect(wrapper.find('img')).to.have.length(1);
  });
   it('should have props for realName', () => {
    const wrapper = shallow(<InfoRow heroInfo={hero}/>);
    expect(wrapper.props().realName).to.be.defined;
  });
   it('should have props for strength', () => {
    const wrapper = shallow(<InfoRow heroInfo={hero}/>);
    expect(wrapper.props().strength).to.be.defined;
  });
});