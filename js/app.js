import React from 'react';
import ReactDOM from 'react-dom';
import { createStore} from 'redux';
import AppContainer from './src/containers/AppContainer';
import Layout from './src/components/Layout';


import HeroRegistration from './src/components/HeroRegistration';
import HeroDetailPage from './src/components/HeroDetailPage';
import EditHero from './src/components/EditHero';
import Login from './src/components/Login';

import reducer from './src/reducers';
import {Provider} from 'react-redux';
import { Router, Route, browserHistory, Link , IndexRoute} from 'react-router';


const store = createStore(reducer);

ReactDOM.render(
  <Provider store={store}>
      <Router history={browserHistory} >
          <Route path="/">
            <IndexRoute component={AppContainer} />
              <Route path="heroDetailPage/:uuid" component={HeroDetailPage}>
               </Route>
                <Route path="heroRegistration" component={HeroRegistration}>
                  </Route>
                  <Route path="editHero" component={EditHero} >
                    </Route>
                    <Route path="login" component={Login}>
                  </Route>
            </Route>
      </Router>
  </Provider>,
  document.getElementById('main')
);

//   ReactDOM.render (
//   <SearchableHeroProfile hero={hero} />,
//   document.getElementById('main')
// )