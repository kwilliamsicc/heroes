import React from 'react';
import ReactDOM from 'react-dom';
import {connect} from 'react-redux';
import { Router, Route, browserHistory, Link , IndexRoute} from 'react-router';

import Layout from '../components/Layout';

import AbilitiesRow from '../components/AbilitiesRow';
import EditHero from '../components/EditHero';
import HeroGroups from '../components/HeroGroups';
import InfoRow from '../components/InfoRow';
import SearchBar from '../components/SearchBar';
import HeroRegistration from '../components/HeroRegistration';
import Header from '../components/Header';
import Footer from '../components/Footer';
import Nav from '../components/Nav';



class AppContainer extends React.Component {

  constructor(props){
    super(props);
    this.init = this.init.bind(this);
    this.initGroup = this.initGroup.bind(this);
  
  }
  
  init(data) {
    console.log("data : " + data);
    let {dispatch} = this.props;
    dispatch({ type: 'INIT', hero: data });
  }

    initGroup(groupData) {
   console.log(groupData);
    let {dispatch} = this.props;
    dispatch({ type: 'INIT_GROUP', heroGroup: groupData });
  }


   componentDidMount() {
    const jq = require('jquery'); 
      jq.getJSON('https://ce3rt0e0yl.execute-api.us-east-1.amazonaws.com/prod/abbHeros', (data) => this.init(data));
      jq.getJSON('https://ce3rt0e0yl.execute-api.us-east-1.amazonaws.com/prod/abbHeroGroups', (groupData) => this.initGroup(groupData));

  }
   
  render() {
    
          
     const listOfHeroes =this.props.hero.map((h,idx) => 

             <InfoRow heroInfo={h} key={idx}/>);

      // const listOfGroups = this.props.heroGroup.map((g,idx) => 

      //        <HeroGroups group={g} key={idx}/>);
   
    return ( 
      <div > 
        <Nav />
        <div className="row">
          <h1> SELECT MUTANT TO VIEW  </h1> 
        {listOfHeroes}
        </div>
      </div>
    );
  }
}

export default connect(state => ({hero :state.hero, heroGroup: state.heroGroup }))(AppContainer);
