export default (state={hero:[], heroGroup:[]}, action) => {
  // console.log(action);
  let oldHero = state.hero;
  switch (action.type) {
    case 'INIT':
    console.log(action.hero)
      return {hero: action.hero, heroGroup: state.heroGroup};
    case 'POST':
      let newHero = Object.assign([], oldHero);
      newHero.push(action.data)
      console.log("newhero : " + action.data)
      return {hero: newHero}
    case 'INIT_GROUP':
    console.log("made it this far")
      return {hero: state.hero, heroGroup: action.heroGroup};
    default:
      return state;
  }
};