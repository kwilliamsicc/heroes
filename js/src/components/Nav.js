import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, browserHistory, Link , IndexRoute} from 'react-router';


export default class Nav extends React.Component {

    render() {
       return (
           <div className="container">
  <nav className="navbar navbar-default navbar-fixed-top">
    <div className="container-fluid">
      <div className="navbar-header">
        <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar1">
          <span className="sr-only">Toggle navigation</span>
          <span className="icon-bar"></span>
          <span className="icon-bar"></span>
          <span className="icon-bar"></span>
        </button>
        <a className="navbar-brand" href="#"><img src="../images/sokovia_accords_logo.png" alt="Dispute Bills"/>
        </a>
      </div>
      <div id="navbar1" className="navbar-collapse collapse">
        <ul className="nav navbar-nav">
          <li><Link to='/heroRegistration' > Register Hero </Link></li>
          <li><Link to='/editHero'> Edit Hero Profile </Link></li> 
          {/*<li className="dropdown">
            <a href="#" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Dropdown <span className="caret"></span></a>
            <ul className="dropdown-menu" role="menu">
              <li><a href="#">Action</a></li>
              <li><a href="#">Another action</a></li>
              <li><a href="#">Something else here</a></li>
              <li className="divider"></li>
              <li className="dropdown-header">Nav header</li>
              <li><a href="#">Separated link</a></li>
              <li><a href="#">One more separated link</a></li>
            </ul>
          </li>*/}
        </ul>
      </div>
      {/*<!--/.nav-collapse -->*/}
    </div>
    {/*<!--/.container-fluid -->*/}
  </nav>
</div>
       );
     }
   }