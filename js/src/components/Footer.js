import React from 'react';
import ReactDOM from 'react-dom';

export default class Footer extends React.Component {

    render() {
       return (
           <tfoot>
                <tr>
                    <td colSpan="12"><b><em>Please Notify Proper Authorities if Mutants Exhibit Dangerous Behavior</em></b></td>
                </tr>
            </tfoot>
       );
    }
}