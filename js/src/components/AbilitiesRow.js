import React from 'react';
import ReactDOM from 'react-dom';

export default class AbilitiesRow extends React.Component {
  
  render() {
    // const abilities = this.props.h.forEach(function(k){
    //   return Object.keys(k.abilities);
    //   return Object.values(k.abilities);
    // });
    // const ratings = this.props.h.forEach(function(v){
    //   return Object.values(v.abilities);
    // });

    return(

      <table>
        <tbody>
            <tr>
                <td>{this.props.fightingSkills}</td>
                <td>{this.props.strength}</td>
                <td>{this.props.durability}</td>
                <td>{this.props.energyProjection}</td>
                <td>{this.props.speed}</td>
                <td>{this.props.intelligence}</td>
             </tr>
        </tbody>
       </table>
    );
  }
}