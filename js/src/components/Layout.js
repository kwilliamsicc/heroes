import React from 'react';
import ReactDOM from 'react-dom';

import Header from './Header';
import Footer from './Footer';
import Nav from './Nav';


export default class Layout extends React.Component {

    render() {
       return (
        <div> 
            <Nav />  
            <Header />
            {this.props.children}
            <Footer />
        </div>
        );
    }
}


