import React from 'react';
import ReactDOM from 'react-dom';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

class HeroRegistration extends React.Component {
    constructor(){
        super();
        this.state = {groups:[], energyProjection:"", strength:"", fightingSkills:"", durability:"", speed:"", intelligence:"", 
        realName:"", s3ImageUrl:"", powers:"", uuid:"", heroName:"", signedAccords:""};
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }
  
  handleChange(event) {
    console.log("refs " + this.refs)
    this.setState({groups:[this.refs.groups.value], energyProjection: this.refs.energyProjection.value, strength: this.refs.strength.value, 
        fightingSkills: this.refs.fightingSkills.value, durability: this.refs.durability.value, speed: this.refs.speed.value, intelligence: this.refs.intelligence.value, 
        realName: this.refs.realName.value , s3ImageUrl: this.refs.s3ImageUrl.value, powers: this.refs.powers.value, uuid: this.refs.uuid.value, heroName: this.refs.heroName.value, signedAccords: this.refs.signedAccords.value});  
  }

  handleSubmit(event) {
    event.preventDefault()
    console.log("this state : " + this.state);
     let{dispatch} = this.props;
      dispatch({type: 'POST', data :this.state});

  $.ajax({
    type: 'POST',
    url: 'http://localhost:8080/heroes',
    data: JSON.stringify(this.state),
    contentType: 'application/json; charset=utf-8',
    dataType: 'json',
    error: function(request, status, error) {
      alert("error : " + error.responseText)
    },
    success: function(data, status) {
      if(data.errorMessage) {
        alert(data.errorMessage);
      } else {
         alert("Your hero has been registered!!")
      }
    },
  })
  .fail(function(jqXhr) {
    alert('failed to register');
    console.log('failed to register');
  });
   this.props.router.push('/');
}

    render() {
        return(
    <div>
        <form onSubmit={this.handleSubmit}>
          <h1> Hero Registration </h1><br/>
            <div className="form-group row">
              <label htmlFor="something" className="col-2 col-form-label"> Image </label>
              <div className="col-10">
            <input className="form-control" type="text" ref="s3ImageUrl" value={this.state.s3ImageUrl} onChange={this.handleChange} />
            </div>
          </div>
            <div className="form-group row">
              <label htmlFor="something" className="col-2 col-form-label"> Real Name </label> 
              <div className="col-10">
           <input className="form-control" type="text" ref="realName" value={this.state.realName} onChange={this.handleChange} /> 
            </div>
          </div>
          <div className="form-group row">
              <label htmlFor="something" className="col-2 col-form-label"> UUID </label>
              <div className="col-10">
            <input className="form-control" type="text" ref="uuid" value={this.state.uuid} onChange={this.handleChange} />
            </div>
          </div>
           <div className="form-group row">
              <label htmlFor="something" className="col-2 col-form-label"> Hero Name </label>
              <div className="col-10">
            <input className="form-control" type="text" ref="heroName" value={this.state.heroName} onChange={this.handleChange} />
            </div>
          </div>
           <div className="form-group row">
              <label htmlFor="something" className="col-2 col-form-label"> Powers </label>
              <div className="col-10">
            <input className="form-control" type="text" ref="powers" value={this.state.powers} onChange={this.handleChange} />
            </div>
          </div>
           <div className="form-group row">
              <label htmlFor="something" className="col-2 col-form-label"> Signed Accords </label>
              <div className="col-10">
            <input className="form-control" type="text" ref="signedAccords" value={this.state.signedAccords} onChange={this.handleChange} />
            </div>
          </div>
           <div className="form-group row">
              <label htmlFor="something" className="col-2 col-form-label"> Fighting Skills </label>
              <div className="col-10">
            <input className="form-control" type="text" ref="fightingSkills" value={this.state.fightingSkills} onChange={this.handleChange}/>
            </div>
          </div>
           <div className="form-group row">
              <label htmlFor="something" className="col-2 col-form-label"> Strength </label>
              <div className="col-10">
            <input className="form-control" type="text" ref="strength" value={this.state.strength} onChange={this.handleChange}/>
            </div>
          </div>
           <div className="form-group row">
              <label htmlFor="something" className="col-2 col-form-label"> Durability </label>
              <div className="col-10">
            <input className="form-control" type="text" ref="durability" value={this.state.durability} onChange={this.handleChange}/>
            </div>
          </div>
          <div className="form-group row">
              <label htmlFor="something" className="col-2 col-form-label"> Speed </label>
              <div className="col-10">
            <input className="form-control" type="text" ref="speed" value={this.state.speed} onChange={this.handleChange} />
            </div>
          </div>
          <div className="form-group row">
              <label htmlFor="something" className="col-2 col-form-label"> Energy Projection </label>
              <div className="col-10">
            <input className="form-control" type="text" ref="energyProjection" value={this.state.energyProjection} onChange={this.handleChange} />
            </div>
          </div>
          <div className="form-group row">
              <label htmlFor="something" className="col-2 col-form-label"> Intelligence </label>
              <div className="col-10">
            <input className="form-control" type="text" ref="intelligence" value={this.state.intelligence} onChange={this.handleChange} />
            </div>
          </div>
           <div className="form-group row">
              <label htmlFor="something" className="col-2 col-form-label"> Group Affiliations </label>
              <div className="col-10">
            <input className="form-control" type="text" ref="groups" value={this.state.groups} onChange={this.handleChange} />
            </div>
          </div>
           <button type="submit" className="btn btn-primary">Submit</button>
        </form>
      </div>
        );
    }

}

export default connect(state => ({hero :state.hero}))(HeroRegistration);