import React from 'react';
import Reach4OM from 'react-dom';
import {connect} from 'react-redux';
import { Router, Route, browserHistory, Link , IndexRoute} from 'react-router';
import {bindActionCreators} from 'redux';


class HeroDetail extends React.Component {
    

    
  
    render() {
        console.log("hey");
        console.log("this props.hero : " + this.props.hero);
        console.log("this props.params .uuid: " + this.props.params.uuid);
   
        let id = this.props.params.uuid;
        let hero= this.props.hero.find(heroObj =>
          heroObj.uuid == id);

         let ep =(hero.abilities.energyProjection/7) * 100 + "%";
         let fs =(hero.abilities.fightingSkills/7) * 100 + "%";
         let s =(hero.abilities.strength/7) * 100 + "%";
         let d =(hero.abilities.durability/7) * 100 + "%";
         let sp =(hero.abilities.speed/7) * 100 + "%";
         let i =(hero.abilities.intelligence/7) * 100 + "%";
         
        return (
            <div>
                    <h4><img style={{"width": 300, height: 250 }}src={hero.s3ImageUrl} /></h4>
            <div className="progress">
                <div className="progress-bar progress-bar-striped active" role="progressbar"
                    aria-valuenow={hero.abilities.energyProjection} aria-valuemin="0" aria-valuemax="7"style={{"width":ep}} >
                   Energy Projection: {hero.abilities.energyProjection}
                </div>
            </div>
            <div className="progress">
                <div className="progress-bar progress-bar-striped active" role="progressbar"
                    aria-valuenow={hero.abilities.fightingSkills} aria-valuemin="0" aria-valuemax="7" style={{"width":fs}}>
                    {hero.abilities.fightingSkills}
                </div>
            </div>
            <div className="progress">
                <div className="progress-bar progress-bar-striped active" role="progressbar"
                    aria-valuenow={hero.abilities.strength} aria-valuemin="0" aria-valuemax="7" style={{"width":s}}>
                    {hero.abilities.strength}
                </div>
            </div>
            <div className="progress">
                <div className="progress-bar progress-bar-striped active" role="progressbar"
                    aria-valuenow={hero.abilities.durability} aria-valuemin="0" aria-valuemax="7" style={{"width":d}}>
                    {hero.abilities.durability}
                </div>
            </div>
            <div className="progress">
                <div className="progress-bar progress-bar-striped active" role="progressbar"
                    aria-valuenow={hero.abilities.speed} aria-valuemin="0" aria-valuemax="7" style={{"width":sp}}>
                    {hero.abilities.speed}
                </div>
            </div>
            <div className="progress">
                <div className="progress-bar progress-bar-striped active" role="progressbar"
                    aria-valuenow={hero.abilities.intelligence} aria-valuemin="0" aria-valuemax="7" style={{"width":i}}>
                    {hero.abilities.intelligence}
                </div>
            </div>
            
                    <h4>{hero.realName}</h4> 
                    <h4 >{hero.heroName}</h4> 
                    <h4 >{hero.powers}</h4> 
                    <h4>{hero.signedAccords}</h4> 
                    <h4>{hero.groups}</h4> 
                    <h4>{hero.abilities.energyProjection}</h4> 
                    <h4>{hero.abilities.fightingSkills}</h4> 
                    <h4>{hero.abilities.strength}</h4> 
                    <h4>{hero.abilities.durability}</h4> 
                    <h4>{hero.abilities.speed}</h4> 
                    <h4>{hero.abilities.intelligence}</h4>  
            </div>
        );
    }
}

export default connect(state => ({hero :state.hero, heroGroup: state.heroGroup }))(HeroDetail);
