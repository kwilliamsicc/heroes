import React from 'react';
import ReactDOM from 'react-dom';

export default class SearchBar extends React.Component {
  render() {
    return (
      <div>
        <h1>SEARCH HERO DATABASE </h1>
        <form>
          <input type="search" placeholder="Search Hero..." />
        </form>
      </div>
    );
  }
}