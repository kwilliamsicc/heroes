import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, browserHistory, Link , IndexRoute} from 'react-router';


export default class InfoRow extends React.Component{
    
    render() {
        return (
              /*<tbody> 
                <tr>
                   <td><img style={{width: 300, height: 250 }}src={this.props.heroInfo.s3ImageUrl}/></td>
                   <td>{this.props.heroInfo.realName}</td> 
                   <td >{this.props.heroInfo.heroName}</td> 
                   <td >{this.props.heroInfo.powers}</td> 
                   <td>{this.props.heroInfo.signedAccords}</td> 
                   <td>{this.props.heroInfo.groups}</td> 
                   <td>{this.props.heroInfo.abilities.energyProjection}</td> 
                   <td>{this.props.heroInfo.abilities.fightingSkills}</td> 
                   <td>{this.props.heroInfo.abilities.strength}</td> 
                   <td>{this.props.heroInfo.abilities.durability}</td> 
                   <td>{this.props.heroInfo.abilities.speed}</td> 
                   <td>{this.props.heroInfo.abilities.intelligence}</td>   
                </tr>
        </tbody> */
  <div className="col-sm-6 col-md-4">
    <div className="thumbnail">
     <Link to={'/heroDetailPage/'+ this.props.heroInfo.uuid}  > 
        <img style={{width: 150, height: 150, marigin: 0 }} src={this.props.heroInfo.s3ImageUrl} className="img-circle" alt="..."/>
     </Link>
      <div className="caption">
        <h3>{this.props.heroInfo.realName} </h3>
        <p> Alias: {this.props.heroInfo.heroName}</p>
        <p><Link to="/nowhere" className="btn btn-default" role="button"> Details</Link></p>
      </div>
    </div>
  </div>

        )
    }
}
       