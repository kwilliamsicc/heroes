import React from 'react';
import ReactDOM from 'react-dom';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import Nav from './Nav';

export default class Login extends React.Component {
    constructor() {
        super();
        this.state = {name:"", password:""};
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    handleChange(event) {
        this.setState({name: this.refs.name.value, password: this.refs.password.value})
    }

    handleSubmit(event) {
        event.preventDefault();
         let name = this.refs.name.value;
        let password = this.refs.password.value;

        if (name.length ==0 || password.length ==0) {
            alert("Please fill input credentials")
        } else if (name != "mrkeithwilliams" || password != "password") {
            alert("Username or Password incorrect");
        }
        else {
           //login to database
        }
    };

    render() {
      const divStyle = {
          color: "white"
      };

        return (
        <div>
            <section id="cover">
                <div id="cover-caption">
                    <div id="container">
                        <div id="col-sm-10 col-sm-offset-1">
                            <h1 style={divStyle} >MUTANT REGISTRATION</h1>
                                 <p style={divStyle}>Sign In Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi quam eum possimus quisquam 
                               provident expedita sequi natus pariatur quas tempora nisi, molestiae, ab voluptatibus explicabo iure 
                               necessitatibus debitis placeat nobis.</p>
                               <form action="" className="form-inline" onSubmit={this.handleSubmit}>
                                   <div className="form-group">
                                       <label htmlFor="" className="sr-only">Name</label>
                                       <input type="text" ref="name" value={this.state.name} onChange={this.handleChange} className="form-control form-control-lg" placeholder="Your Name"/>
                                   </div>
                                   <div className="form-group">
                                       <label htmlFor="" className="sr-only">Password</label>
                                       <input type="text" ref="password" value={this.state.password} onChange={this.handleChange} className="form-control form-control-lg" placeholder="Your Password"/>
                                   </div>
                                     <button type="submit" className="btn btn-success-btn-lg">SIGN IN</button>
                                </form>
                               <br/>

                               <a href="#nav-main" className="btn btn-secondary-outline btn-sm" role="button">&darr;</a>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        );
    }
}