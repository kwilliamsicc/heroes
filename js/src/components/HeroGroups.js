import React from 'react';
import ReactDOM from 'react-dom';
import {connect} from 'react-redux';
import { Router, Route, browserHistory, Link , IndexRoute} from 'react-router';

class HeroGroups extends React.Component {

  constructor(props){
    super(props);
   
  } 
  
  render() {

    return ( <ul>
      <img style={{width: 300, height: 250 }} src={this.props.group.s3ImageUrl}/>
      <li>{this.props.group.uuid} </li> 
      <div id="map" style={{width : 320, height: 480}}>{this.props.group.location} </div> 
      <li>{this.props.group.description} </li>  
      </ul> 
    );
  }
}

export default connect(state => ({hero :state.hero, heroGroup: state.heroGroup }))(HeroGroups);


